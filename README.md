# Fedifinder

The fediverse friend finder is now open source.\
This new OOTB ready local testing version is available to you under the AGPL.\
Fedifinder software runs on [Django](https://www.djangoproject.com/)


How to run:

Install Python3 or have it installed\
Change to a directory to hold the project files\
Optionally create a venv for the project with the commands:\
python3 -m venv {Name for the virtual environment}\
source {Name for the virtual environment}/bin/activate\
python3 -m pip install --upgrade pip\
Clone the project\
Install the package dependencies with the command:\
pip install -r requirements.txt\
The package versions are not the minimum required, other versions may work but are untested\
Change directory to fedifinderdev folder\
Run the command:\
python3 manage.py runserver\
Open http://localhost:8000 in your web browser


Login:

I have pre-made an admin account for you\
Visit: http://localhost:8000/admin \
Email: admin@example.tld\
Password: password\
This will let you use the site as you wish without a fedi account.\
Once you add your fedi account, you can use this admin user to\
give your fedi account admin privileges from the admin interface.


Instances:

The following instances already have an application included for local use:\
noauthority.social , \
mstdn.social , \
spinster.xyz , \
mk.nixnet.social \
If you want to use a different server you will have to make a new app and add the client + key.\
Check `match/keydict.py` for a note on this.
