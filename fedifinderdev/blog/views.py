from django.shortcuts import render
from .models import Post


# Views
def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})


def blog(request):
    context = {
        'posts': Post.objects.all().order_by('-date_posted'),
        'title': 'Blog'
    }
    return render(request, 'blog/blog.html', context)


def splash(request):
    return render(request, 'blog/splash.html', {'title': 'Home'})
