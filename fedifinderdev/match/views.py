from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.timezone import datetime
from django.utils.safestring import mark_safe
from urllib.parse import unquote
from math import cos, asin, sqrt, pi
from .forms import InstanceForm, RequestForm, ProfileUpdateForm, SearchForm
from .keydict import key_dict, mk_ins, inst_alias, inst_alias_rev, chk_str, chk_usr
from .models import fedi_user
from .gallery import gal_list
import requests

model_attr_list = ['age', 'attract', 'avatar', 'bio', 'email', 'header', 'height_ft', 'height_in', 'interests',
                   'join_fedi', 'join_match', 'location', 'loc_lat', 'loc_lon', 'mbti', 'occupation', 'orient',
                   'lang_known', 'lang_learn',
                   'photos', 'pol_soc', 'pol_eco', 'quiz', 'race', 'reli', 'rel_stat', 'seek', 'sex', 'weight',
                   'uhand', 'uhost', 'username']


def home(request: HttpRequest) -> JsonResponse:
    return render(request, "blog/splash.html")


def test(request):
    user = request.user
    if user.is_authenticated:
        context = {"msg": "Authenticated"}
        for key in model_attr_list:
            context[key] = getattr(user, key)
        context['XYZ'] = user.xyz
        return JsonResponse(context)
    else:
        context = {"Error": "You are unauthenticated, please log in"}
        return render(request, "match/generic_error.html", context)


@login_required(login_url="/find/login")
def stats(request):
    user = request.user
    if user.email == "HLC@noagendasocial.com":
        cl = key_dict.keys()
        cll = []
        for inst in cl:
            if len(fedi_user.objects.filter(uhost=inst)) == 0:
                cll.append(inst)
        context = {"msg": "Authenticated",
                   "total": str(len(fedi_user.objects.all())),
                   "ghosts": str(len(fedi_user.objects.filter(age=0))),
                   "social_only": str(len(fedi_user.objects.filter(seek=1))),
                   "romantic_only": str(len(fedi_user.objects.filter(seek=2))),
                   "sex_none": str(len(fedi_user.objects.filter(sex=0))),
                   "sex_male": str(len(fedi_user.objects.filter(sex=1))),
                   "sex_female": str(len(fedi_user.objects.filter(sex=2))),
                   "sausage_quotient": str(len(fedi_user.objects.exclude(seek=1).filter(sex=1).filter(rel_stat__in=[1, 4, 5]))) + "/" + str(len(fedi_user.objects.exclude(seek=1).filter(sex=2).filter(rel_stat__in=[1, 4, 5]))),
                   "host_NAS": str(len(fedi_user.objects.filter(uhost="noagendasocial.com"))),
                   "empty_instances": str(cll),
                   "userlist": str(list(fedi_user.objects.all().values_list('email', flat=True)))
                  }
        return JsonResponse(context)
    else:
        context = {"Error": "You are unauthenticated, or are not allowed to view this page"}
        return render(request, "match/generic_error.html", context)


@login_required(login_url="/find/login")
def get_images(request):
    user = request.user
    if user.email == "HLC@noagendasocial.com":
        whole = fedi_user.objects.filter(join_match__gte=datetime.fromtimestamp(1643501000))
        avi_list = []
        for user in whole:
            avi_list.append((user.email, user.avatar, user.url_loc))
        context = {"msg": "Authenticated",
                   "list": str(avi_list),
                  }
        return JsonResponse(context)
    else:
        context = {"Error": "You are unauthenticated, or are not allowed to view this page"}
        return render(request, "match/generic_error.html", context)


@login_required(login_url="/find/login")
def gallery(request):
    user = request.user
    if user.is_authenticated:
        context = {"photos": gal_list}
        return render(request, "match/gallery.html", context)


@login_required(login_url="/find/login")
def update_images(request, *args, **kwargs):
    user = request.user
    if user.is_authenticated:
        user.xyz = "U"
        user.save()
        logout(request)
        return render(request, "match/update_images.html")
    else:
        context = {"Error": "You are unauthenticated, please log in"}
        return render(request, "match/generic_error.html", context)


@login_required(login_url="/find/login")
def delete(request, *args, **kwargs):
    user = request.user
    if user.is_authenticated:
        context = {"msg": user.id}
        if kwargs:
            conf_split = kwargs.get("conf").split(sep='-')
            if conf_split[0] == "forreal" and int(conf_split[1]) == user.id:
                fedi_user.objects.filter(id=user.id).delete()
                print("Account #" + str(user.id) + " deleted")
                return JsonResponse({"Success": "Account deleted"})
            else:
                context["Error"] = "Bad deletion confirmation string"
                return render(request, "match/generic_error.html", context)
        else:
            return render(request, "match/delete.html", context)
    else:
        context = {"Error": "You are unauthenticated, please log in"}
        return render(request, "match/generic_error.html", context)


@login_required(login_url="/find/login")
def browse(request, *args, **kwargs):
    user = request.user
    context = {'title': 'Browse'}
    if kwargs:
        valid_cat = ['new', 'near', 'match-soc', 'match-rom', 'lang']
        category = kwargs.get("main")
        if category not in valid_cat:
            context["Error"] = "Bad browse string"
            return render(request, "match/generic_error.html", context)
        context['subtitle'] = category
        sub_cat = kwargs.get("sec")
        q_set = fedi_user.objects.none()  # Start with blank queryset
        if sub_cat:
            try:
                sub_cat_val = int(sub_cat)
            except ValueError:
                context["Error"] = "Bad browse sub-string"
                return render(request, "match/generic_error.html", context)
            if category == "new":  # 'New' category (Recently joined fedifinder)
                context['time'] = sub_cat_val
                # Calculate time frame window in epoch time (sub_cat_val in hours aka 3600s)
                time_threshold = datetime.fromtimestamp(datetime.now().timestamp() - (3600 * sub_cat_val))
                q_set = search_logic(["join_match_min", time_threshold], q_set).exclude(id=user.id)
                q_set = q_set.order_by('-join_match')  # Order entries by most recently joined
                for prof_obj in q_set:  # Temporary change to profile fields to display data
                    prof_obj.weight = 1234567
                    prof_obj.report = "Joined FF: "
                    prof_obj.xyz = prof_obj.join_match
            elif category == "lang":  # Language category (People who share a common language, known or learning)
                if 5 > sub_cat_val > 0:  # Unlike with 'New' and 'Near', this cannot be any number, valid #s are 1,2,3,4
                    context["group"] = sub_cat_val  # Group is flag for sub-nav-bar highlighting like 'time' and 'distance'
                else:  # If URL sub-cat value is out of the 1-4 range return error page
                    context["Error"] = "Bad browse sub-string"
                    return render(request, "match/generic_error.html", context)
                lang_dict = {}
                if user.lang_known is None:  # If user hasn't specified known languages ('is None' necessary)
                    if (sub_cat_val == 1 or sub_cat_val == 4) and lang_dict.get('known') is None:  # And sub-category requires it
                        return render(request, "match/browse.html", context)  # Return page without results
                else:  # Otherwise process data
                    if "," in user.lang_known:  # If field has a comma, ie a list of languages
                        lang_dict['known'] = user.lang_known.split(sep=",")  # Separate csv string into list and add to dict
                    else:  # Otherwise just add the single language item
                        lang_dict['known'] = [user.lang_known]  # Add as list with length 1 to use common for loop
                if user.lang_learn is None:  # Repeat for language(s) being learned
                    if (sub_cat_val == 2 or sub_cat_val == 3) and lang_dict.get('learn') is None:
                        return render(request, "match/browse.html", context)
                else:
                    if "," in user.lang_learn:
                        lang_dict['learn'] = user.lang_learn.split(sep=",")
                    else:
                        lang_dict['learn'] = [user.lang_learn]
                set_dict = {}  # Dictionary of {'Language': [Queryset of profiles with matching language]}
                b_set = fedi_user.objects.none()  # Intentionally blank set to pass to search_logic
                if sub_cat_val == 1:  # Sub-category 1 is users who know the same language(s)
                    lmr = "Knows: "  # "Language Match Reason" to display to user, variable by sub_cat
                    for lang in lang_dict['known']:  # For each language listed in lang_known
                        set_dict[lang] = search_logic(["lang_known", lang], b_set).exclude(id=user.id)
                        q_set = q_set.union(set_dict[lang])  # Incorporate that set into the larger q_set
                elif sub_cat_val == 2:  # Sub-category 2 is users who are learning the same language(s)
                    lmr = "Learning: "
                    for lang in lang_dict['learn']:
                        set_dict[lang] = search_logic(["lang_learn", lang], b_set).exclude(id=user.id)
                        q_set = q_set.union(set_dict[lang])
                elif sub_cat_val == 3:  # Sub-category 3 is users who know some language(s) that the user is learning
                    lmr = "Knows: "
                    for lang in lang_dict['learn']:
                        set_dict[lang] = search_logic(["lang_known", lang], b_set).exclude(id=user.id)
                        q_set = q_set.union(set_dict[lang])
                else:  # Sub-category 4 is users who are learning some language(s) that the user knows
                    lmr = "Learning: "
                    for lang in lang_dict['known']:
                        set_dict[lang] = search_logic(["lang_learn", lang], b_set).exclude(id=user.id)
                        q_set = q_set.union(set_dict[lang])
                for prof_obj in q_set:  # Temporary change to profile fields to display data
                    prof_obj.weight = 1234567
                    prof_obj.report = lmr
                    for set_n in set_dict:  # For each entry n in the set dictionary
                        if prof_obj in set_dict[set_n]:  # Check if the result profile is in that set
                            if len(prof_obj.xyz) > 1:  # If there is already a matching language specified
                                prof_obj.xyz += ", " + set_n  # Extend display data into list of languages
                            else:  # Otherwise just display the one matching language
                                prof_obj.xyz = set_n
            else:  # 'Nearest/near' only other category with sub_cat
                context['subtitle'] = category
                context['distance'] = sub_cat_val
                # loc_dis, val, loc_uni, val, loc_lat, val, loc_lon, val: values as strings parsed in function
                near_list = ["loc_dis", sub_cat, "loc_uni", "0", "loc_lat", user.loc_lat, "loc_lon", user.loc_lon]
                q_set = search_logic(near_list, q_set).exclude(id=user.id)
                for prof_obj in q_set:  # Temporary change to profile fields to display data
                    prof_obj.weight = 1234567
                    prof_obj.report = "Distance from you: "
                    distance = haversine(float(user.loc_lat), float(user.loc_lon), float(prof_obj.loc_lat), float(prof_obj.loc_lon))
                    prof_obj.xyz = str(round(distance*0.6214, 1)) + " mi (" + str(round(distance, 1)) + " km)"
        else:
            if category == 'match-rom':
                if user.seek == 1 or user.rel_stat == 3:  # Social only or married are excluded
                    context["Error"] = "Your account settings preclude you from using this function (Social only or Married)"
                    return render(request, "match/generic_error.html", context)
                else:
                    # Begin with query set that excludes social only, in relationship, married users, and self.
                    base_set = fedi_user.objects.exclude(seek=1, rel_stat__in=[2, 3]).exclude(id=user.id)
                    if user.orient == 1:  # Hetero
                        q_set = base_set.filter(orient=1)
                        if user.sex == 1:  # Man -> Woman
                            q_set = q_set.filter(sex=2)
                        elif user.sex == 2:  # Woman -> Man
                            q_set = q_set.filter(sex=1)
                        elif user.sex == 3:  # TRNM -> NT, TRNF, NB
                            q_set = q_set.filter(sex__in=[0, 4, 5])
                        elif user.sex == 4:  # TRNM -> NT, TRNM, NB
                            q_set = q_set.filter(sex__in=[0, 3, 5])
                        elif user.sex == 5:  # NB -> NT, TRNM, TRNF, NB
                            q_set = q_set.filter(sex__in=[0, 3, 4, 5])
                        # No filter for sex = 0 aka NT
                    elif user.orient == 2:  # Homo
                        q_set = base_set.filter(orient__in=[2, 3])
                        if user.sex == 1 or user.sex == 2:  # Man/Woman -> Same
                            q_set = q_set.filter(sex=user.sex)
                        elif user.sex > 2:  # TRNM/F, NB -> NT, Same
                            q_set = q_set.filter(sex__in=[0, user.sex])
                        # No filter for sex = 0 aka NT
                    elif user.orient == 3:  # Other
                        q_set = base_set.filter(orient__in=[0, 2, 3])
            else:  # match-soc
                if user.seek == 2:  # Romantic only are excluded from using
                    context["Error"] = "Your account settings preclude you from using this function (Romantic only)"
                    return render(request, "match/generic_error.html", context)
                else:
                    # Begin with query set that excludes romantic only and user (self)
                    base_set = fedi_user.objects.exclude(seek=2).exclude(id=user.id)
                    i_set = r_set = o_set = m_set = p_set = j_set = a_set = s_set = fedi_user.objects.none()
                    cur_len = 0
                    set_list = []
                    if user.interests:
                        if "," in user.interests:
                            int_split = user.interests.split(sep=",")
                            int_dict = {}
                            for item in int_split:
                                filtered = base_set.filter(interests__icontains=item.strip())
                                if len(filtered) > 0:
                                    for int_match in filtered:
                                        int_dict[int_match.email] = item
                                i_set = i_set.union(filtered)
                        else:
                            i_set = base_set.filter(interests__icontains=user.interests)
                        cur_len = cur_len + len(i_set)
                        set_list.append(i_set)
                    if user.mbti:
                        typelist = ['INFP', 'ENFP', 'INFJ', 'ENFJ', 'INTJ', 'ENTJ', 'INTP', 'ENTP',
                                    'ISFP', 'ESFP', 'ISTP', 'ESTP', 'ISFJ', 'ESFJ', 'ISTJ', 'ESTJ']
                        if user.mbti in typelist:
                            typedict = {'INFP': [3, 5], 'ENFP': [2, 4], 'INFJ': [1, 7], 'ENFJ': [0, 8], 'INTJ': [1, 7], 'ENTJ': [0, 6],
                                        'INTP': [5, 15], 'ENTP': [2, 4], 'ISFP': [3, 13, 15], 'ESFP': [12, 14], 'ISTP': [13, 15],
                                        'ESTP': [12, 14], 'ISFJ': [9, 11], 'ESFJ': [8, 10], 'ISTJ': [9, 11], 'ESTJ': [6, 8, 10]}
                            mbtimatch = typedict.get(user.mbti)
                            for match in mbtimatch:
                                m_set = m_set.union(base_set.filter(mbti=typelist[match]))
                        cur_len = cur_len + len(m_set)
                        set_list.append(m_set)
                    if user.reli:  # Matching PART of religious denomination eg. * Christian or Orthodox *
                        if " " in user.reli:
                            reli_split = user.reli.split(sep=" ")
                            for item in reli_split:
                                r_set = r_set.union(base_set.filter(reli__icontains=item))
                        else:
                            r_set = base_set.filter(reli__icontains=user.reli)
                        cur_len = cur_len + len(r_set)
                        set_list.append(r_set)
                    if user.occupation:  # Matching PART of occupation title, eg variations kinds of * artist or * engineer
                        if " " in user.occupation:
                            occ_split = user.occupation.split(sep=" ")
                            for item in occ_split:
                                o_set = o_set.union(base_set.filter(occupation__icontains=item))
                        else:
                            o_set = base_set.filter(occupation__icontains=user.occupation)
                        cur_len = cur_len + len(o_set)
                        set_list.append(o_set)
                    if user.pol_soc != 0 or user.pol_eco != 0:  # Closeness on the political compass (+/- 1 vertical and horizontal)
                        lb_s = user.pol_soc - 1
                        ub_s = user.pol_soc + 1
                        lb_e = user.pol_eco - 1
                        ub_e = user.pol_eco + 1
                        p_set = base_set.exclude(pol_soc=0.00, pol_eco=0.00).filter(pol_soc__range=(lb_s, ub_s)).filter(pol_eco__range=(lb_e, ub_e))
                        cur_len = cur_len + len(p_set)
                        set_list.append(p_set)
                    if cur_len < 10:  # If the result list is less than 10 entries try less relevant match criteria
                        # Calculate time frame window in epoch time (+/- 3 months) for fediverse join date
                        tt_lower = datetime.fromtimestamp(user.join_fedi.timestamp() - 8035200)
                        tt_upper = datetime.fromtimestamp(user.join_fedi.timestamp() + 8035200)
                        j_set = base_set.filter(join_fedi__gte=tt_lower).filter(join_fedi__lte=tt_upper)
                        cur_len = cur_len + len(j_set)
                        set_list.append(j_set)
                    if cur_len < 10 and user.age != 0:  # Within +/- 4 year window of age, if they have it specified
                        a_set = base_set.filter(age__gte=user.age - 4).filter(age__lte=user.age + 4)
                        cur_len = cur_len + len(a_set)
                        set_list.append(a_set)
                    if cur_len < 10 and user.rel_stat != 0:  # Someone with the same relationship status
                        s_set = base_set.filter(rel_stat=user.rel_stat)
                        set_list.append(s_set)
                    q_set = fedi_user.objects.none()
                    for ind_set in set_list:  # Combine all sets into one
                        q_set = q_set.union(ind_set)
                    q_set = q_set.order_by('-join_match')  # Order entries by most recently joined
                    for prof_obj in q_set:  # Temporary change to profile fields to display data
                        prof_obj.weight = 1234567
                        if prof_obj in i_set:  # Display diff reason based on set
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Shared interest (" + int_dict[prof_obj.email] + ")"
                        elif prof_obj in m_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "MBTI type compatibility (" + prof_obj.mbti + ")"
                        elif prof_obj in r_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Similar religion (" + prof_obj.reli + ")"
                        elif prof_obj in o_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Similar occupation (" + prof_obj.occupation + ")"
                        elif prof_obj in p_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Political compass proximity (" + str(prof_obj.pol_eco) + "," + str(prof_obj.pol_soc) + ")"
                        elif prof_obj in j_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Joined fedi around the same time (" + str(prof_obj.join_fedi) + ")"
                        elif prof_obj in a_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Close in age to you (" + str(prof_obj.age) + ")"
                        elif prof_obj in s_set:
                            prof_obj.report = "Match reason: "
                            prof_obj.xyz = "Same relationship status as you (" + prof_obj.rel_stat + ")"
        # Emojify accounts, if there are any
        if len(q_set) > 0:
            for account in q_set:
                if account.uhand.count(':') > 1 and account.uhand.count(':') % 2 == 0:
                    account.username = mark_safe(emojify(account.uhand, account.uhost))
                    account.uhand = "emojified"
        context["accounts"] = q_set
        # Result for empty sets handled in list_accounts.html logic
        return render(request, "match/browse.html", context)
    else:
        context['subtitle'] = "default"
        return render(request, "match/browse.html", context)


@login_required(login_url="/find/login")
def quiz_view(request, *args, **kwargs):
    user = request.user
    if kwargs:  # If the view is loaded with /<instance>/<user>
        inst_string = str(kwargs.get("inst")).lower()
        try:  # Check instance abbreviation in URL against dict
            inst_url = inst_alias[inst_string]
        except KeyError:
            context = {"Error": "The instance abbreviation '" + inst_string + "' didn't match any site"}
            return render(request, "match/generic_error.html", context)
        unamestr = kwargs.get("uname") + "@" + inst_url  # Create email from username and instance name
        find_user = fedi_user.objects.filter(email=unamestr).values()  # Check DB for user with given email
        if len(find_user) == 0:
            context = {"Error": "User '" + unamestr + "' was not found in the database"}
            return render(request, "match/generic_error.html", context)
        else:  # Emails are unique and filter is exact so queryset will have only one value
            context = {}
            profile = list(find_user).pop()
            if profile['quiz'] and profile['quiz'][:3] == "[q]":
                quiz_list = profile['quiz'].split(sep="[q]")[1:]  # First entry is '' if text starts with [q]
                # print(quiz_list)
            else:
                context = {"Error": "This user does not have a quiz or it is improperly formatted"}
                return render(request, "match/generic_error.html", context)
            if request.POST:  # Not enough data cleaning to justify figuring out why formsets don't work right
                # print("Request")
                # print(request.POST)
                # print(profile['email'])
                msg_str = "Hey @" + profile['email'] + " you just got a response to your quiz from @" + user.email + "\r\n"
                for i in range(0, len(quiz_list)):
                    question = quiz_list[i]
                    if not question.endswith('\r\n'):  # Add new line to end of question if it does not already have one
                        question += '\r\n'
                    qnum = "q" + str(i+1)  # 0 indexed forloop.counter doesn't work for some reason
                    answer = request.POST[qnum]
                    if len(answer.strip()) == 0:  # Skip any question that wasn't answered
                        continue
                    while answer.endswith('\r\n'):  # Remove all trailing new lines from the answer
                        answer = answer[:-2]  # \r\n counts as two characters rather than 4
                    msg_str += question + answer + '\r\n'
                msg_str += "Love it? Hate it? Let them know what you think!"
                # print(msg_str)
                ret_msg = send_message(msg_str)
                if ret_msg == "success":
                    ret_dict = {'ret_stat': 'Success', 'details': 'Your message was sent'}
                else:
                    ret_dict = {'ret_stat': 'Failure', 'details': ret_msg}
                return render(request, "match/quiz_return.html", ret_dict)
            else:
                context['quiz'] = quiz_list
                total_len = 0
                for que in quiz_list:  # Find the total length of quiz questions so answers can be as long as possible
                    total_len += len(que)
                context['ans_len'] = int((4560-total_len)/len(quiz_list))  # Take from 4560 to leave 440 for head/foot
                context['ans_row'] = context['ans_len']/100  # Adjust the number of rows in each textarea by length
                context['for'] = profile['email']
            context['title'] = "Quiz"
            return render(request, "match/quiz.html", context)
    else:  # If the view is loaded without kwargs
        context = {"Error": "No profile given. Use /find/quiz/<Instance Abbreviation>/<username>"}
        return render(request, "match/generic_error.html", context)


def emojify(uhand: str, uhost: str):
    if uhost == "noagendasocial.com":
        new_hand = uhand.replace(":verified:", '<img draggable="false" alt=":verified:" title=":verified:" width=16px height=16px src="https://static.noagendasocial.com/custom_emojis/images/000/113/194/static/6a100070f83f906c.png">')
    else:
        em_split = uhand.split(":")
        new_hand = ""
        for i in range(0, len(em_split)):
            if i % 2 == 1:
                ex_sec = em_split[i]
                new_hand += '<img draggable="false" alt=":' + ex_sec + ':" title=":' + ex_sec + ':" width=16px height=16px src="https://' + \
                    uhost + '/emoji/custom/' + ex_sec + '.png">'
            else:
                new_hand += em_split[i]
    return new_hand


@login_required(login_url="/find/login")
def profile_view(request, *args, **kwargs):
    user = request.user

    def create_gallery(linkset):
        linelist = linkset.split(sep="\r\n")
        # print(linelist)
        gallery_list = []
        for line in linelist:
            if line.startswith("[img]"):
                tag = line[:5]
                link = line[5:]
            elif line.startswith("[post]"):
                tag = line[:6]
                link = line[6:]
            else:
                tag = "[fail]"
                link = None
            gallery_list.append((tag, link))
        return gallery_list

    def create_context(prof, cont):
        for key in prof:  # Cycle through all profile attributes
            if key in model_attr_list:  # Extract display elements
                cont[key] = prof[key]  # Use only those as context
        if prof['photos']:
            gallery = create_gallery(prof['photos'])
            del cont['photos']
            cont['photos'] = gallery
        if prof['quiz']:
            ql = prof['quiz'].split('[q]')  # Split quiz into a list so it can be displayed 1 question per line
            del cont['quiz']
            cont['quiz'] = ql  # Blank entry from leading '[q]' doesn't render, no need to remove
            cont['quiz_url'] = prof['url_loc'].replace('user', 'quiz')
        if cont['uhand'].count(':') > 0 and cont['uhand'].count(':') % 2 == 0:
            cont['emoji'] = mark_safe(emojify(cont['uhand'], cont['uhost']))
            cont['uhand'] = "emojified"
        return cont

    if kwargs:  # If the view is likely loaded from /<instance>/<user>
        if kwargs.get("inst") and kwargs.get("uname"):  # If the view is definitely loaded from /<instance>/<user>
            # print("Profile page: Direct route to profile")
            inst_string = str(kwargs.get("inst")).lower()
            try:  # Check instance abbreviation in URL against dict
                inst_url = inst_alias[inst_string]
            except KeyError:
                context = {"Error": "The instance abbreviation '" + inst_string + "' didn't match any site"}
                return render(request, "match/generic_error.html", context)
            unamestr = kwargs.get("uname") + "@" + inst_url  # Create email from username and instance name
            find_user = fedi_user.objects.filter(email=unamestr).values()  # Check DB for user with given email
            if len(find_user) == 0:
                context = {"Error": "User '" + unamestr + "' was not found in the database"}
                return render(request, "match/generic_error.html", context)
            else:  # Emails are unique and filter is exact so queryset will have only one value
                profile = list(find_user).pop()
                # print(profile)
                context = {}
                context = create_context(profile, context)
                return render(request, "match/view_account.html", context)
        else:  # If the view was loaded with kwargs, but not the correct ones somehow
            context = {"Error": "Malformed profile url. Use /find/user/<Instance Abbreviation>/<username>"}
            return render(request, "match/generic_error.html", context)
    else:  # If the view is loaded from /me without kwargs
        # print("Profile page: '/me' shortcut")
        find_user = fedi_user.objects.filter(id=user.id).values()
        profile = list(find_user).pop()
        context = {}
        context = create_context(profile, context)
        context['title'] = "Me"  # Add title for navigation bar
        return render(request, "match/view_account.html", context)


@login_required(login_url="/find/login")
def edit_account(request):
    user = request.user
    try:
        profile = fedi_user.objects.get(pk=user.id)
    except fedi_user.DoesNotExist:
        return HttpResponse("Account retrieval failed")
    if user.id != profile.id:
        return HttpResponse("It looks like you're trying to edit someone else's account")
    context = {}
    initial_values = {}
    for key in model_attr_list:  # Prepare initial set of values for the form fields
        initial_values[key] = getattr(profile, key)
    # print(initial_values)
    if request.POST:
        form = ProfileUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            # print("Save happened")
            return redirect("match-profile-me")
        else:
            initial_values['check'] = ['height_ft', 'height_in', 'loc_lat', 'loc_lon', 'pol_eco', 'pol_soc', 'weight']  # Add field for form template to check and display errors if available
            form = ProfileUpdateForm(request.POST, instance=request.user, initial=initial_values)
            context['form'] = form
    else:
        form = ProfileUpdateForm(request.POST, instance=request.user, initial=initial_values)
        context['form'] = form
    context['title'] = "Edit"
    return render(request, "match/edit_account.html", context)


@login_required(login_url="/find/login")
def search_in(request):
    context = {}
    if request.POST:
        form = SearchForm(request.POST)
        if form.is_valid():
            sp = form.search()
            # print(sp)
            if len(sp) > 1950:
                return HttpResponse("You created a search string that was too long. Please limit your search.")
            else:
                return redirect("/find/search/" + sp)
        else:
            form = SearchForm(request.POST)
            context['form'] = form
    else:
        form = SearchForm(request.POST)
        context['form'] = form
    context['title'] = "Search"  # Add title for navigation bar
    return render(request, "match/search_input.html", context)


@login_required(login_url="/find/login")
def search_out(request, *args, **kwargs):
    if kwargs:
        # Break URL kwarg into parts (SS,search_string,checksum)
        src_str_split = kwargs.get("src_str").split(sep="&&", maxsplit=2)
        # Check that the search string conforms to valid parameters
        try:
            chk_sum = chk_str(src_str_split[0] + "&&" + src_str_split[1])
        except IndexError:
            # print("Bad search string")
            chk_sum = 9999
        if len(src_str_split) == 3 and src_str_split[0] == "SS" and src_str_split[2] == chk_sum:
            # print("Confirmed search string")
            search_string = src_str_split[1]
            content = {"Full string": kwargs.get("src_str"), "Search string": search_string}
            # Break search string into parts (& denotes new search category)
            search_list = search_string.split(sep="&")
            search_key_list = []
            search_val_list = []
            for item in search_list:
                # Break search category into key,value pair and add to list
                src_lst_split = item.split(sep="=")
                # Unquote value if it contains %-encoded character
                if "%" in src_lst_split[1]:
                    stv = unquote(src_lst_split[1])
                else:
                    stv = src_lst_split[1]
                content[src_lst_split[0]] = stv
                search_key_list.append(src_lst_split[0])
                search_val_list.append(stv)
            # Cycle through search elements
            q_set = fedi_user.objects.none()  # blank queryset
            i = 0
            search_len = len(search_key_list)
            while i in range(0, search_len):
                # print(search_key_list[i] + " = " + search_val_list[i])
                # Check search parameters for search element pairs (sequential, same name except for last 3 chars)
                if i != search_len-1 and search_key_list[i][:-3] == search_key_list[i+1][:-3]:
                    # Check if this pair is part of a four element set (and that list is large enough to check 3 ahead)
                    if len(search_key_list) > i+3 and search_key_list[i+1][:-3] == search_key_list[i+2][:-3] == search_key_list[i+3][:-3]:
                        # print("Four Parter: " + search_key_list[i][:-4])
                        search_pass = [search_key_list[i], search_val_list[i], search_key_list[i + 1], search_val_list[i + 1],
                                       search_key_list[i + 2], search_val_list[i + 2], search_key_list[i + 3], search_val_list[i + 3]]
                        i += 4
                    else:
                        # print("Two Parter: " + search_key_list[i][:-4])
                        search_pass = [search_key_list[i], search_val_list[i], search_key_list[i+1], search_val_list[i+1]]
                        i += 2
                else:  # All other search queries are processed individually
                    search_pass = [search_key_list[i], search_val_list[i]]
                    i += 1
                q_set = search_logic(search_pass, q_set)
            # print(q_set)
            # Cycle through results in queryset, empty set is handled in HTML
            accounts = []
            for account in q_set:
                if account.uhand.count(':') > 0 and account.uhand.count(':') % 2 == 0:
                    account.username = mark_safe(emojify(account.uhand, account.uhost))
                    account.uhand = "emojified"
                accounts.append(account)
            context = {"accounts": accounts, "title": "Search"}
            return render(request, "match/search_output.html", context)
        else:
            context = {"Error": "Malformed search query. Use form to pick parameters."}
            return render(request, "match/generic_error.html", context)
    else:
        context = {"Error": "No search string provided"}
        return render(request, "match/generic_error.html", context)


def logout_user(request):
    logout(request)
    return redirect("match-home")


def request_message(request: HttpRequest):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = RequestForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # Get instance domain from form.cleaned_data
            req_msg = str(form.cleaned_data['request_msg'])
            if len(req_msg) > 10:
                msg_str = "@HLC@noagendasocial.com you have received a request for inclusion:\r\n"
                msg_str += req_msg
                # print(msg_str)
                ret_msg = send_message(msg_str)
                if ret_msg == "success":
                    ret_dict = {'ret_stat': 'Success', 'details': 'Your message was sent'}
                else:
                    ret_dict = {'ret_stat': 'Failure', 'details': ret_msg}
                return render(request, "match/quiz_return.html", ret_dict)
            else:  # If form data is invalid, return form with error message
                messages.add_message(request, messages.ERROR, mark_safe("ERROR: Your message was likely a mistake as it was <10 characters long.<br>You are allowed up to 2000 characters."))
                return render(request, 'match/request_form.html', {'form': form})
    # if a GET (or any other method), create a blank form
    else:
        form = RequestForm()
    return render(request, 'match/request_form.html', {'form': form})


def login_user(request: HttpRequest):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = InstanceForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # Get instance domain from form.cleaned_data
            usr_inst_url = str(form.cleaned_data['instance_url'])
            if usr_inst_url in key_dict:
                if usr_inst_url in mk_ins:  # In Misskey login URLs are generated with the app secret - making them variable
                    token_url = "https://" + usr_inst_url + "/api/auth/session/generate"
                    print(token_url)
                    response = requests.post(token_url, json={'appSecret': key_dict[usr_inst_url][1]})
                    print(response)
                    auth_res = response.json()
                    oauth_url = auth_res['url']
                    print("OAuth URL: " + oauth_url)
                else:  # In Pleroma and Mastodon login URLs are generated with the client id - making it the same each time
                    # Create oauth url from user instance and associated client id
                    oauth_url = "https://" + usr_inst_url + "/oauth/authorize?client_id=" + key_dict[usr_inst_url][0] + "&scope=read:accounts&redirect_uri=http://localhost:8000/find/login/redirect/" + usr_inst_url + "&response_type=code"
                    print("User instance: " + usr_inst_url)
                    print("OAuth URL: " + oauth_url)
                return redirect(oauth_url)  # redirect to oauth url:
            else:  # If form data is invalid, return form with error message
                ims = ""
                for key in key_dict:
                    ims += str(key) + "<br/>"
                messages.add_message(request, messages.ERROR, mark_safe("ERROR: '" + usr_inst_url + "' is not an approved instance. Try again.<br/> Available instances:<br/>" + ims))
                return render(request, 'match/instance_form.html', {'form': form})
    # if a GET (or any other method), create a blank form
    else:
        form = InstanceForm()
    return render(request, 'match/instance_form.html', {'form': form})


def login_redirect(request: HttpRequest, *args, **kwargs):
    ref = kwargs.get("iname")
    if not ref or ref not in inst_alias_rev:
        context = {"Error": "Your login redirect link was not assigned"}
        return render(request, "match/generic_error.html", context)
    if ref in mk_ins:
        code = request.GET.get("token")
        print("Token (Returned):")
        print(code)
        token_url = "https://" + ref + "/api/auth/session/userkey"
        response = requests.post(token_url, json={'appSecret': key_dict[ref][1], "token": code})
        usr_json = response.json()
        print("User JSON")
        print(usr_json)
        print("refined")
        user = refine_data_mk(ref, usr_json['user'])
        print(user)
    else:
        print("Ref: " + ref)
        code = request.GET.get("code")
        print("Code: " + code)
        user = exchange_code(code, ref)
    if user['email'] in chk_usr:  # Check if account is on DNE list
        return HttpResponse("Account retrieval failed.")
    fedizen = authenticate(request, **user)
    print("User authenticated: " + str(fedizen))
    login(request, fedizen)
    print("Login occurred")
    if fedizen.xyz == "Y":  # Check for first time creating account
        # print("New user, redirecting to update form")
        return redirect("match-edit")
    elif fedizen.xyz == "U":  # Check for image update flag
        fedizen.header = user['header']
        fedizen.avatar = user['avatar']
        fedizen.xyz = "Z"
        fedizen.save()
        return redirect("match-profile-me")
    else:
        print("Existing user redirecting to profile")
        return redirect("match-profile-me")


def exchange_code(code: str, ref: str):
    data = {
        "client_id": key_dict[ref][0],
        "client_secret": key_dict[ref][1],
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": "http://localhost:8000/find/login/redirect/" + ref,
        "scope": "read:accounts"
    }
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    print("Data packet:")
    print(data)
    token_url = "https://" + ref + "/oauth/token"
    print("Token URL:")
    print(token_url)
    response = requests.post(token_url, data=data, headers=headers)
    print("Token Response:")
    print(response)
    credentials = response.json()
    print("Credentials:")
    print(credentials)
    access_token = credentials['access_token']
    print("Access token:")
    print(access_token)
    headers = {
        "Authorization": "Bearer %s" % access_token
    }
    print("Header:")
    print(headers)
    acct_url = "https://" + ref + "/api/v1/accounts/verify_credentials"
    response = requests.get(acct_url, headers=headers)
    print("Ver_Cred Response:")
    print(response)
    user = refine_data(response.json())
    user['email'] = user['acct'] + "@" + ref
    user['ref'] = ref
    if "?name=" in user['avatar']:
        user['avatar'] = user['avatar'].split('?name=')[0]
    if "?name=" in user['header']:
        user['header'] = user['header'].split('?name=')[0]
    print("User:")
    print(user)
    return user


def refine_data(user: dict):
    # Reduce user data from bearer token response to relevant fields only
    key_list = ['acct', 'avatar', 'created_at', 'display_name', 'header', 'note', 'url']
    newuser = {}
    for key in user:
        if key in key_list:
            if not user[key]:
                newuser[key] = ""
            else:
                newuser[key] = user[key]
    return newuser


def refine_data_mk(ref: str, user: dict):
    # Remake user data in the form which would be made by exchange_code
    newuser = {
        'email': user['username'] + "@" + ref, 'ref': ref,
        'acct': user['username'], 'avatar': user['avatarUrl'], 'created_at': user['createdAt'], 'display_name': user['name'],
        'header': user['bannerUrl'], 'note': user['description'], 'url': "https://" + ref + "/@" + user['username']
    }
    for key in newuser:
        if not newuser[key]:
            newuser[key] = ""
    return newuser


def haversine(lat1, lon1, lat2, lon2):
    p = pi / 180
    a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a))


def search_logic(st: list, qs):
    valid_keys = ["seek", "sex", "rel_stat", "orient", "quad", "uhand", "username", "uhost",
                  "attract", "race", "reli", "mbti", "bio", "interests", "quiz", "location", "occupation",
                  "join_fedi_min", "join_fedi_max", "join_match_min", "join_match_max",
                  "height_ft_min", "height_ft_max", "height_in_min", "height_in_max",
                  "lang_known", "lang_learn", "weight_min", "weight_max", "age_min", "age_max"]

    if len(qs) == 0:  # If query set is empty, search in the set of all model entries
        sqs = fedi_user.objects
    else:  # If the query set is not empty, search within the query set to refine results
        sqs = qs
    if len(st) > 2:  # If the search category has at least two parts eg min/max
        if len(st) > 4:  # If the search has more than two parts ie location search
            # print("Four parter received")
            # print(st)

            # loc_dis, val, loc_uni, val, loc_lat, val, loc_lon, val
            dist = float(st[1])  # Distance in km or mi
            llat = float(st[5])
            llon = float(st[7])
            # Approximate degrees per mi and km
            if st[3] == '0':
                unit = 0.0145
            else:
                unit = 0.009
            # Initially exclude non-entries and anyone out of range by lat dist (always same ratio of deg/dist) and lon distance (biggest deg/dist ratio at poles)
            bqs = sqs.exclude(loc_lat=0, loc_lon=0).filter(loc_lat__lt=llat+(dist*unit), loc_lat__gt=llat-(dist*unit)).filter(loc_lon__lt=llon+(dist*unit*57.25), loc_lon__gt=llon-(dist*unit*57.25))
            rqs = fedi_user.objects.none()  # blank queryset
            accounts = []
            if len(bqs) > 0:
                for account in bqs:
                    distance = haversine(llat, llon, float(account.loc_lat), float(account.loc_lon))
                    if st[3] == '0':  # Result of function is in km, adjust to mi if requested
                        distance = distance * 0.6214
                    if distance < dist:
                        accounts.append(account.id)
                    else:
                        continue
                rqs = bqs.filter(id__in=accounts)  # Returning list messes up order_by, so query ids instead
                return rqs
            else:
                return rqs
        if st[0] in valid_keys and st[2] in valid_keys:
            # print(st)
            if st[0] == "join_fedi_min":
                rqs = sqs.filter(join_fedi__gte=st[1]).filter(join_fedi__lte=st[3])
            elif st[0] == "join_match_min":
                rqs = sqs.filter(join_match__gte=st[1]).filter(join_match__lte=st[3])
            elif st[0] == "height_ft_min":
                rqs = sqs.filter(height_ft__gte=st[1]).filter(height_ft__lte=st[3])
            elif st[0] == "height_in_min":
                rqs = sqs.filter(height_in__gte=st[1]).filter(height_in__lte=st[3])
            elif st[0] == "weight_min":
                rqs = sqs.filter(weight__gte=st[1]).filter(weight__lte=st[3])
            elif st[0] == "age_min":
                rqs = sqs.filter(age__gte=st[1]).filter(age__lte=st[3])
            else:
                return qs
        else:
            return qs
    else:
        if st[0] in valid_keys:
            # print(st)
            if st[0] == "seek":
                if st[1] == "3":  # 3 represents "any" option, i.e. no filter
                    return qs
                else:
                    rqs = sqs.filter(seek=st[1])
            elif st[0] == "sex":
                if st[1] == "6":  # 6 represents "any" option, i.e. no filter
                    return qs
                else:
                    rqs = sqs.filter(sex=st[1])
            elif st[0] == "rel_stat":
                if st[1] == "6":  # 6 represents "any" option, i.e. no filter
                    return qs
                else:
                    rqs = sqs.filter(rel_stat=st[1])
            elif st[0] == "orient":
                if st[1] == "4":  # 4 represents "any" option, i.e. no filter
                    return qs
                else:
                    rqs = sqs.filter(orient=st[1])
            elif st[0] == "quad":
                if st[1] == "0":  # 0 represents "any" option, i.e. no filter
                    return qs       # Eco X Axis Soc Y Axis
                elif st[1] == "1":  # 1 represents Auth. Left
                    rqs = sqs.filter(pol_eco__lt=0).filter(pol_soc__gt=0)
                elif st[1] == "2":  # 2 represents Auth. Right
                    rqs = sqs.filter(pol_eco__gt=0).filter(pol_soc__gt=0)
                elif st[1] == "3":  # 3 represents Lib. Left
                    rqs = sqs.filter(pol_eco__lt=0).filter(pol_soc__lt=0)
                elif st[1] == "4":  # 4 represents Lib. Right
                    rqs = sqs.filter(pol_eco__gt=0).filter(pol_soc__lt=0)
                else:
                    return qs
            elif st[0] == "uhand":
                rqs = sqs.filter(uhand__icontains=st[1])
            elif st[0] == "username":
                rqs = sqs.filter(username__icontains=st[1])
            elif st[0] == "uhost":
                rqs = sqs.filter(uhost__icontains=st[1])
            elif st[0] == "attract":
                rqs = sqs.filter(attract__icontains=st[1])
            elif st[0] == "race":
                rqs = sqs.filter(race__icontains=st[1])
            elif st[0] == "lang_known":
                rqs = sqs.filter(lang_known__icontains=st[1])
            elif st[0] == "lang_learn":
                rqs = sqs.filter(lang_learn__icontains=st[1])
            elif st[0] == "reli":
                rqs = sqs.filter(reli__icontains=st[1])
            elif st[0] == "mbti":
                rqs = sqs.filter(mbti__icontains=st[1])
            elif st[0] == "bio":
                rqs = sqs.filter(bio__icontains=st[1])
            elif st[0] == "interests":
                rqs = sqs.filter(interests__icontains=st[1])
            elif st[0] == "quiz":
                rqs = sqs.filter(quiz__icontains=st[1])
            elif st[0] == "location":
                rqs = sqs.filter(location__icontains=st[1])
            elif st[0] == "occupation":
                rqs = sqs.filter(occupation__icontains=st[1])
            elif st[0] == "join_fedi_min":
                rqs = sqs.filter(join_fedi__gte=st[1])
            elif st[0] == "join_fedi_max":
                rqs = sqs.filter(join_fedi__lte=st[1])
            elif st[0] == "join_match_min":
                rqs = sqs.filter(join_match__gte=st[1])
            elif st[0] == "join_match_max":
                rqs = sqs.filter(join_match__lte=st[1])
            elif st[0] == "height_ft_min":
                rqs = sqs.filter(height_ft__gte=st[1])
            elif st[0] == "height_ft_max":
                rqs = sqs.filter(height_ft__lte=st[1])
            elif st[0] == "height_in_min":
                rqs = sqs.filter(height_in__gte=st[1])
            elif st[0] == "height_in_max":
                rqs = sqs.filter(height_in__lte=st[1])
            elif st[0] == "weight_min":
                rqs = sqs.filter(weight__gte=st[1])
            elif st[0] == "weight_max":
                rqs = sqs.filter(weight__lte=st[1])
            elif st[0] == "age_min":
                rqs = sqs.filter(age__gte=st[1])
            elif st[0] == "age_max":
                rqs = sqs.filter(age__lte=st[1])
            else:
                return qs
        else:
            return qs
    return rqs


def send_message(message: str):
    data = {
        "status": message,
        "visibility": "direct"
    }
    headers = {
        "Authorization": "Bearer abcdefghijklmnopqrstuvwxyz123456789"
    }
    url = "https://noagendasocial.com/api/v1/statuses"
    response = requests.post(url, data=data, headers=headers)
    chk_res = response.json()
    try:
        error = chk_res['error']
    except KeyError:  # No key error, no 'error' field in return JSON, no problem
        error = "none"
    if error != "none":
        # print("MESSAGE NOT SENT")
        if error == "The status is over the character limit":
            ret_str = "Error: Response message was too long"
        else:
            ret_str = "Error: " + error
    else:
        # print("MESSAGE SENT")
        ret_str = "success"
    return ret_str


def fix_bio(text: str):
    startcut = 0
    endcut = 0
    startlink = 0
    endlink = 0

    t1 = text.replace('<br />', '\r\n').replace('<br/>', '\r\n').replace('<p>', '\r\n').replace('</p>', '\r\n')  # Replace breaks
    t2 = t1.replace('<span class="h-card">', '').replace('<span>', '').replace('</span>', '')  # Remove spans

    while '</a>' in t2:  # Replace <a> blocks with just link text
        for i in range(0, len(t2)):
            if t2[i] == '<':
                if t2[i + 1:i + 3] == 'a ':
                    startcut = i
                    for c in range(i + 3, len(t2)):
                        if t2[c:c + 6] == 'href="':
                            startlink = c + 6
                            break
                    for n in range(startlink, len(t2)):
                        if t2[n:n + 2] == '" ':
                            endlink = n
                            break
                    for j in range(endlink, len(t2)):
                        if t2[j:j + 4] == '</a>':
                            endcut = j + 4
                            break
                    t2 = t2[:startcut] + t2[startlink:endlink] + t2[endcut:]
                    break

    # Replace some common HTML encoded characters
    htmlcodes = (
        ("'", '&#39;'),
        ("'", '&apos;'),
        ('"', '&quot;'),
        ('&', '&amp;')
    )
    for code in htmlcodes:
        t2 = t2.replace(code[1], code[0])

    t3 = t2.strip()

    return t3
