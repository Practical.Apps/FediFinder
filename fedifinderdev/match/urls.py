from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='match-home'),
    path('test/', views.test, name='match-test'),
    path('stats/', views.stats, name='match-stats'),
    path('avis/', views.get_images, name='match-avis'),
    path('gallery/', views.gallery, name='match-gallery'),
    path('browse/', views.browse, name='match-browse-base'),
    path('browse/<str:main>/', views.browse, name='match-browse-one'),
    path('browse/<str:main>/<str:sec>', views.browse, name='match-browse-two'),
    path('search/', views.search_in, name='match-search-in'),
    path('search/<str:src_str>', views.search_out, name='match-search-out'),
    path('quiz/', views.quiz_view, name='match-quiz'),
    path('quiz/<str:inst>/<str:uname>', views.quiz_view, name='match-quiz'),
    path('user/<str:inst>/<str:uname>', views.profile_view, name='match-profile'),
    path('me/', views.profile_view, name='match-profile-me'),
    path('edit/', views.edit_account, name='match-edit'),
    path('udim/', views.update_images, name='match-update-images'),
    path('request/', views.request_message, name='match-request-message'),
    path('delete/', views.delete, name='match-delete'),
    path('delete/<str:conf>', views.delete, name='match-delete-conf'),
    path('logout/', views.logout_user, name='match-logout'),
    path('login/', views.login_user, name='match-login'),
    path('login/redirect/<str:iname>', views.login_redirect, name='match-login-redirect'),
    path('login/redirect', views.login_redirect, name='match-login-redirect'),
]
