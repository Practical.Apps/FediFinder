key_dict = {# Format = Domain: [Client ID, Client Secret]
    'noauthority.social': ["61u8ml93A90B6zuHP5mCKMi6zwrnqL5tphNVDYb4_aA", "1cpU1KYznIZa82c6GzTxFqYcz9j14hW1T16fmSebwTk"],
    'mstdn.social': ["x3XOJ_0ZYmOdiGEM-PyYQ5mrQkIHOdacTnbEA5mGgpA", "BmsEe-nwAxPeGDw7zH-gxVVNfGfp-5guvMn4Gj5V4X4"],
    'spinster.xyz': ["D5OdO4H5uDpzQtLypnLyIv9aMXGCYqIYhcYotk3jKig", "m2czB1jOhrAiFUA5jzE2IO6h6sVVTw0bHhV2MfMkcYQ"],
    'mk.nixnet.social': ["MK:mk.nixnet.social", "76qQIh7i3MCh9rqlPhEEuDXjetaq1YVO"]  # This server is gone but retained as an example for Misskey
}
# These app keys redirect to http://localhost:8000/find/login/redirect/{instance_url} and are for local testing
# Production apps redirect to https://www.fedifinder.party/find/login/redirect/{instance_url}
# To use another instance you will have to create an application with read:accounts as the scope
# And set the redirect URL to the localhost example above with your instance URL applied
# e.g. http://localhost:8000/find/login/redirect/noauthority.social
# And add your instance to the following lists as well as the instance_form.html page in match templates

inst_alias = {
    'nas': 'noauthority.social',
    'mst': 'mstdn.social',
    'spn': 'spinster.xyz',
    'mnn': 'mk.nixnet.social'
}

inst_alias_rev = {
    'noauthority.social': 'nas',
    'mstdn.social': 'mst',
    'spinster.xyz': 'spn',
    'mk.nixnet.social': 'mnn'
}

# List of Misskey Instances which need a slightly different login procedure
mk_ins = ['mk.nixnet.social']

chk_usr = [
    # This section is for a list of banned users
]


def chk_str(com: str):
    print("CHK: " + com)
    chk_sum = 0
    # This function is meant as a checksum for the search function
    # I have not included the actual function for security purposes
    # The point is to prevent SQL injection by ensuring search queries
    # were made with the site's search function.
    # Search for its usage in views.py to see how it works,
    # returning zero should work fine for local testing.
    chk_sum_str = str(chk_sum)
    print(chk_sum_str)
    return chk_sum_str
