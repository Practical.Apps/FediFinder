from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import fedi_user
from django import forms


class FediAdmin(UserAdmin):
    list_display = ('email', 'username', 'join_match', 'last_login', 'is_admin', 'is_staff')
    search_fields = ('email', 'username')
    readonly_fields = ('id', 'join_match', 'last_login')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

    # Change Bio/Photo/Quiz fields in Admin interface to multi-line
    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super(FediAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name in ['bio', 'photos', 'quiz']:
            formfield.widget = forms.Textarea(attrs=formfield.widget.attrs)
        return formfield


admin.site.register(fedi_user, FediAdmin)
