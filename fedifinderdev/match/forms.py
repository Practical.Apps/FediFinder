from django import forms
from django.utils.http import urlencode
from urllib.parse import quote, unquote
from .models import fedi_user
from .keydict import chk_str, inst_alias_rev


class InstanceForm(forms.Form):
    instance_url = forms.CharField(label='Instance URL', max_length=75)


class RequestForm(forms.Form):
    request_msg = forms.CharField(label='Request Message', max_length=75)


class SearchForm(forms.Form):

    seek = forms.IntegerField(required=False)
    sex = forms.IntegerField(required=False)
    rel_stat = forms.IntegerField(required=False)
    orient = forms.IntegerField(required=False)
    quad = forms.IntegerField(required=False)
    uhand = forms.CharField(max_length=75, required=False)
    username = forms.CharField(max_length=75, required=False)
    uhost = forms.CharField(max_length=75, required=False)
    attract = forms.CharField(max_length=50, required=False)
    race = forms.CharField(max_length=50, required=False)
    reli = forms.CharField(max_length=50, required=False)
    mbti = forms.CharField(max_length=4, required=False)
    bio = forms.CharField(max_length=100, required=False)
    interests = forms.CharField(max_length=100, required=False)
    quiz = forms.CharField(max_length=100, required=False)
    lang_known = forms.CharField(max_length=50, required=False)
    lang_learn = forms.CharField(max_length=50, required=False)
    location = forms.CharField(max_length=50, required=False)
    loc_dis = forms.IntegerField(required=False)
    loc_uni = forms.IntegerField(required=False)
    loc_lat = forms.DecimalField(decimal_places=12, max_digits=14, required=False)
    loc_lon = forms.DecimalField(decimal_places=12, max_digits=15, required=False)
    occupation = forms.CharField(max_length=50, required=False)
    join_fedi_min = forms.CharField(max_length=10, required=False)
    join_fedi_max = forms.CharField(max_length=10, required=False)
    join_match_min = forms.CharField(max_length=10, required=False)
    join_match_max = forms.CharField(max_length=10, required=False)
    height_ft_min = forms.IntegerField(required=False)
    height_in_min = forms.IntegerField(required=False)
    height_ft_max = forms.IntegerField(required=False)
    height_in_max = forms.IntegerField(required=False)
    weight_min = forms.IntegerField(required=False)
    weight_max = forms.IntegerField(required=False)
    age_min = forms.IntegerField(required=False)
    age_max = forms.IntegerField(required=False)

    def search(self):
        print("Got to search")
        search_string = "SS%26%26"
        search_params = {}
        for key in self.cleaned_data:
            if self.cleaned_data[key] != "" and self.cleaned_data[key] != None:  # Shorthand skips integers with value 0
                search_params[key] = self.cleaned_data[key]
        search_string += quote(urlencode(search_params))
        search_string += "%26%26" + chk_str(unquote(search_string))
        return search_string


class ProfileUpdateForm(forms.ModelForm):

    class Meta:
        model = fedi_user
        fields = ('age', 'attract', 'bio', 'height_ft', 'height_in', 'interests', 'lang_known', 'lang_learn',
                  'location', 'loc_lat', 'loc_lon', 'mbti', 'occupation', 'orient', 'photos',
                  'pol_eco', 'pol_soc', 'quiz', 'race', 'reli', 'rel_stat', 'seek', 'sex', 'uhand', 'weight')

    def clean_photos(self):
        data = self.cleaned_data.get('photos')
        # print("Raw: " + str(data))
        cleanbool = False
        cdnlist = ['static.banky.club', 'img.poast.org', 'l.poastcdn.org']  # Alternative list for CDN with different URL from instance
        cleanlist = cdnlist + list(inst_alias_rev.keys())  # CDN list first so break happens before instance detected
        if data and data != "None":  # Blank text box forms were sometimes populated with "None" as text
            linelist = data.split(sep="\r\n")
            print(linelist)
            for line in linelist:
                if line.startswith("[img]"):
                    link = line[5:]
                elif line.startswith("[post]"):
                    link = line[6:]
                else:
                    print("Your lines must be prepended with [img] or [post].")
                    raise forms.ValidationError("Your lines must be prepended with [img] or [post].")
                for key in cleanlist:
                    if key in link:  # Check that link has on 2 slashes before instance name (Not scam.xyz/instance.com)
                        test = link.split(key)  # And that preceding char precludes tweaked domains (123instance.com)
                        if test[0].count('/') == 2 and test[0][-1] in ['/', '.']:
                            cleanbool = True
                            break  # Should trigger before hitting the SPC part of https://static.banky.club/shitposter.club/yadayada
                    else:
                        cleanbool = False
                if not cleanbool:
                    print("Your photos/posts cannot come from unapproved instances. Sorry.")
                    raise forms.ValidationError("Your photos/posts cannot come from unapproved instances. Sorry.")
            return data
        else:
            return data

    def save(self, commit=True):
        fedi_user = super(ProfileUpdateForm, self).save(commit=False)
        for key in self.cleaned_data:  # cycle through all fields in form
            fedi_user.key = self.cleaned_data[key]  # update the user object values
        if fedi_user.xyz == "Y":
            fedi_user.xyz = "Z"
        if commit:
            fedi_user.save()
        return fedi_user
